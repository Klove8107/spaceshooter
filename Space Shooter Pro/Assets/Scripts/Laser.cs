﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    //speed variable
    [SerializeField]
    private float _speed = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        Update();
    }

    // Update is called once per frame
    void Update()
    {
        //translate laser up

        transform.Translate(Vector3.up * _speed * Time.deltaTime);

        //if laser position is greater that 8 on y destroy the object
        if (transform.position.y > 8f)
        {
            Destroy(this.gameObject);
        }
    }
}
