﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class Player : MonoBehaviour
{
    //c# datatpes (int, float, bool, string) public variables are displayed on unity ui private variables are denoted with _
    //serialized  feilds allow you to view privte variables in ui
    [SerializeField]
    private float _speed = 3.5f;
    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private float _fireRate = 0.15f;
    [SerializeField]
    private float _canFire = -1f;


    // Start is called before the first frame update
    void Start()
    {
        //take the current position and assign it a start position = new position (x,y,z)
        transform.position = new Vector3(0, 0, 0);

    }

    // Update is called once per frame typically at 60fps
    //1 unit = 1 meter in the real world
    void Update()
    {
        CalculateMovement();
        LaserShot();
          
        
    }

    void CalculateMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontalInput, verticalInput, 0);

        //this takes sprite quickly to the right 
        // Time.deltaTime = 1 second or time in seconds from last to current frame
        //every time you use the new keyword your program is less optimized

        transform.Translate(direction * _speed * Time.deltaTime);

        //get the the player position on the y to stay in bounds 
        //if player position is greater than 0 y = 0

        float positiony = transform.position.y;
        float positionx = transform.position.x;

        //instead of writing if statements you can  do this to constrain objects within bounds
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -4.5f, 6.5f), 0);

        if(positionx >= 11.3f)
        {
            transform.position = new Vector3(-11.3f, positiony, 0);

        }
        else if (positionx <= -11.3f)
        {
            transform.position = new Vector3(11.3f, positiony, 0);
        }

    }

    void LaserShot()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire)
        {
            //new vector specifies offset
            _canFire = Time.time + _fireRate;
            Instantiate(_laserPrefab, transform.position + new Vector3(0,0.8f,0), Quaternion.identity);
        }
    }
}
